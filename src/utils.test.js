import {
  areCardsSameRank,
  areCardsSameSuit,
  canReinforceWithCard,
  getCardsForReinforcement,
  canDefend,
  getCardForPassing,
  canCardBeat,
  compareCards,
  handCardsSorted
} from "./utils";

describe("Stupid game algorigthm", () => {
  it("Should say if two cards are same suit", () => {
    expect(areCardsSameSuit("H5", "D2")).toEqual(false);
    expect(areCardsSameSuit("H5", "H2")).toEqual(true);
  });

  it("Should say if two cards are same ranks", () => {
    expect(areCardsSameRank("H5", "D2")).toEqual(false);
    expect(areCardsSameRank("D2", "H2")).toEqual(true);
  });

  it("Should calculate card can beat", () => {
    expect(canCardBeat("H", "H5", "D2")).toEqual(true);
    expect(canCardBeat("H", "H5", "H4")).toEqual(true);
    expect(canCardBeat("D", "H5", "H4")).toEqual(true);
    expect(canCardBeat("C", "D5", "H2")).toEqual(false);
    expect(canCardBeat("D", "HK", "HA")).toEqual(false);
    expect(canCardBeat("D", "C2", "S2")).toEqual(false);
    expect(canCardBeat("D", "DK", "DA")).toEqual(false);
  });

  it("Should be able to make reinforcement", () => {
    expect(getCardsForReinforcement("H", ["D4", "DQ", "DA"], ["S2", "D2"], ["S4", "CK", "SA"])).toEqual(["D4", "DA"]);
    expect(getCardsForReinforcement("D", ["DA", "DQ", "D4"], ["S2"], ["S4", "CK", "SA"])).toEqual(["D4"]);
    expect(getCardsForReinforcement("S", ["D7", "DQ", "DJ"], [""], ["S4", "CK", "SA"])).toEqual([]);
  });

  it("Should be able to tell if can reinforce with card", () => {
    expect(canReinforceWithCard("D4", ["S4", "CK", "SA"])).toEqual(true);
    expect(canReinforceWithCard("D2", ["S4", "CK", "SA"])).toEqual(false);
  });

  it("Should be able to calculate if hand can defend", () => {
    expect(canDefend("H", ["HK", "CA", "S3", "S5"], ["S4", "CK", "SA"])).toEqual(true);
    expect(canDefend("S", ["HK", "CA", "S3", "S5"], ["S4", "CK", "SA"])).toEqual(false);
    expect(canDefend("D", ["HK", "CA", "S3", "S5"], ["S4", "CK", "SA"])).toEqual(false);
  });

  it("Should compare two cards", () => {
    expect(compareCards("H", "D2", "HA")).toEqual(-1);
    expect(compareCards("H", "D2", "SA")).toEqual(-1);
    expect(compareCards("H", "H2", "C3")).toEqual(1);
    expect(compareCards("D", "H2", "S2")).toEqual(-1);
    expect(compareCards("D", "SK", "H2")).toEqual(1);
    expect(compareCards("D", "H5", "C2")).toEqual(1);
  });

  it("Should sort cards", () => {
    expect(handCardsSorted("D", ["H5", "D2", "C2", "HK", "SK", "DA", "H2", "S7", "CK", "CA", "CT"])).toEqual([
      "H2", "C2", "H5", "S7", "CT", "HK", "CK", "SK", "CA", "D2", "DA"
    ]);
  });

  it("Should make propper passings", () => {
    expect(getCardForPassing("D", ["D2"], ["H2", "S2"], [])).toEqual(false);
    expect(getCardForPassing("C", ["H2"], ["C2", "D2"], ["S2"])).toEqual(false);
    expect(getCardForPassing("H", ["H2", "HK"], ["C2"], ["S2"])).toEqual("C2");
    expect(getCardForPassing("C", ["H2", "HK"], ["C2", "D2"], ["S2"])).toEqual("D2");
    expect(getCardForPassing("H", ["HQ", "H7", "HK"], ["S2", "H6", "C7", "S6", "H5"], ["C2"])).toEqual("S2");
  });
});
