import {
  log,
  isHandEmpty,
  getCardForAttack,
  getCardsForDefence,
  handWithoutCards,
  getCardsForReinforcement,
  canDefend,
  getCardForPassing
} from "./utils";

export const Passing = (data) => {
  log("State: ");
  log(data);
  let { trump, player1, player2, turn = [], attacker = 1 } = data;

  let attack = attacker === 1 ? player1 : player2;
  let defence = attacker === 1 ? player2 : player1;

  if (getCardForPassing(trump, attack, defence, turn)) {
    const passingWithCard = getCardForPassing(trump, attack, defence, turn);
    log(`Passing against ${turn[turn.length - 1]} with ${passingWithCard}`);
    turn.push(passingWithCard);

    defence = handWithoutCards(defence, passingWithCard);

    player1 = attacker === 1 ? attack : defence;
    player2 = attacker === 1 ? defence : attack;

    data = Passing({ trump, player1, player2, turn, attacker: attacker === 1 ? 2 : 1 });
  }

  return data;
};

export const Attack = (data) => {
  log("State: ");
  log(data);
  const { trump, player1, player2, turn = [], attacker = 1 } = data;
  const offence = attacker === 1 ? player1 : player2;
  const card = getCardForAttack(trump, offence);

  log(`Player ${attacker} attacks with ${card}`);

  turn.push(card);

  data.turn = turn;
  if (attacker === 1) {
    data.player1 = handWithoutCards(data.player1, card);
  } else {
    data.player2 = handWithoutCards(data.player2, card);
  }

  return data;
};

export const Defence = (data) => {
  log("State: ");
  log(data);
  let { trump, player1, player2, turn = [], attacker = 1, defended = [] } = data;

  let attack = attacker === 1 ? player1 : player2;
  let defence = attacker === 1 ? player2 : player1;

  log(`Try to defend ${turn} with ${defence}`);

  if (canDefend(trump, defence, turn) && turn.length > 0) {
    const defenceCards = getCardsForDefence(trump, defence, turn);
    log(`Defend ${turn} with ${defenceCards}`);
    defended = [...defended, ...turn, ...defenceCards];
    turn = [];
    defence = handWithoutCards(defence, ...defenceCards);

    const reinforcement = getCardsForReinforcement(trump, attack, defence, defended);
    attack = handWithoutCards(attack, ...reinforcement);

    player1 = attacker === 1 ? attack : defence;
    player2 = attacker === 1 ? defence : attack;

    if (reinforcement.length > 0) {
      log(`Reinforce with ${reinforcement}`);
      turn = reinforcement;

      return Defence({ trump, player1, player2, turn, defended, attacker });
    }

    log("Successfully defended, switch turn.");

    return { trump, player1, player2, turn, defended: [], attacker: attacker === 1 ? 2 : 1 };
  }

  log("Cannot defend");

  return data;
};

export const DeclareWinner = ({ player1, player2 }) => {
  if (isHandEmpty(player1) && isHandEmpty(player2)) {
    log("Game finished draw");
    return 0;
  } else if (isHandEmpty(player1)) {
    log("Player 1 won the game");
    return 1;
  } else if (isHandEmpty(player2)) {
    log("Player 2 won the game");
    return 2;
  }

  return -1;
};

export const Turn = (input, count = 1) => {
  const result = DeclareWinner(input);
  if (result > -1) {
    return result;
  }

  log(`Turn #${count}`);

  return Turn(Defence(Passing(Attack(input))), count + 1);
};
