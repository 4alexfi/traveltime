import fs from "fs";

const ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"];
const suits = ["H", "D", "C", "S"];

export const getCardSuit = (card) => card[0];
export const getCardRank = (card) => card[1];

export const areCardsSameSuit = (card1, card2) => getCardSuit(card1) === getCardSuit(card2);
export const areCardsSameRank = (card1, card2) => getCardRank(card1) === getCardRank(card2);
export const isHandEmpty = (hand) => typeof Array.isArray(hand) && hand.length === 0;

export const canDefend = (trump, hand, turn) => getCardsForDefence(trump, hand, turn).length === turn.length;
export const canReinforceWithCard = (card, cards) => cards.filter(c => areCardsSameRank(c, card)).length > 0;
export const canCardBeat = (trump, card1, card2) => (
  (getCardSuit(card1) === trump && getCardSuit(card2) !== trump) ||
    (areCardsSameSuit(card1, card2) && ranks.indexOf(getCardRank(card1)) > ranks.indexOf(getCardRank(card2)))
);

export const handCardsSorted = (trump, cards) => cards.sort((c1, c2) => compareCards(trump, c1, c2));
export const handWithoutCards = (hand, ...cards) => hand.filter(c => !cards.includes(c));
export const handWithCards = (hand, cards) => [...hand, ...cards];

export const compareCards = (trump, card1, card2) => {
  if (canCardBeat(trump, card1, card2)) {
    return 1;
  } else if (canCardBeat(trump, card2, card1)) {
    return -1;
  }

  if (areCardsSameRank(card1, card2)) {
    return suits.indexOf(getCardSuit(card1)) > suits.indexOf(getCardSuit(card2)) ? 1 : -1;
  } else {
    return ranks.indexOf(getCardRank(card1)) > ranks.indexOf(getCardRank(card2)) ? 1 : -1;
  }
};

export const getCardForAttack = (trump, hand) => handCardsSorted(trump, hand)[0] || false;

export const getCardForPassing = (trump, hand1, hand2, turn) => {
  if (turn.length > 0 && hand1.length > turn.length) {
    const cardsCanPass = handCardsSorted(trump, hand2).filter((c) => areCardsSameRank(c, turn[0]));
    return cardsCanPass[0] || false;
  }
  return false;
};

export const getCardsForReinforcement = (trump, hand1, hand2, cards) => {
  const reinforcement = handCardsSorted(trump, hand1).filter(card => canReinforceWithCard(card, cards));
  return reinforcement.slice(0, hand2.length);
};

export const getCardsForDefence = (trump, hand, turn) => {
  let defence = [];

  turn.filter(turnCard => {
    handCardsSorted(trump, hand).some((card) => {
      if (canCardBeat(trump, card, turnCard)) {
        hand = handWithoutCards(hand, card);
        defence.push(card);
        return true;
      }
    });
  });

  return defence;
};

export const log = console.log; // eslint-disable-line no-console

export const saveGameResult = (result, output) => {
  fs.appendFile(output, result, (err) => {
    if (err) {
      console.log(err);
    }
  });
};
