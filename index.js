import {
  log,
  saveGameResult
} from "./src/utils";

import { Turn } from "./src/moves";

import fs from "fs";
import readline from "readline";

const inputFile = process.env.npm_config_inputFile;
const outputFile = process.env.npm_config_outputFile;

if (!inputFile) {
  log("Please user --inputFile parameter to specify data input file");
  process.exit();
}

if (!outputFile) {
  log("Please user --outputFile parameter to specify data output file");
  process.exit();
}

const rd = readline.createInterface({
  input: fs.createReadStream(inputFile),
  console: false
});

let trump;
let gameCount = 0;

rd.on("line", (line) => {
  if (!trump) {
    trump = line;
    return;
  }

  const hands = line.split(" | ");

  if (hands.length === 2) {
    gameCount++;

    log(`Game #${gameCount}`);

    const result = Turn({
      trump,
      player1: hands[0].split(" "),
      player2: hands[1].split(" ")
    });

    saveGameResult(result, outputFile);
  }
});
